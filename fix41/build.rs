
fn main() {
    let out_dir = ::std::env::var("OUT_DIR").unwrap();

    ::nfix_codegen::generate_from_spec(
            ::nfix_spec::FIX41_XML,
            &format!("{}/fix41.rs", out_dir))
        .unwrap();
}
