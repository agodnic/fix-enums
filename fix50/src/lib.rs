#![recursion_limit="256"]

include!(concat!(env!("OUT_DIR"), "/fix50.rs"));

#[test]
fn ord_status() {

    let status = crate::OrdStatus::REJECTED;
    assert_eq!(crate::OrdStatus::REJECTED.to_char(), '8');

    let res = crate::OrdStatus::from_char('8');
    assert!(res.is_ok());
    assert_eq!(res.unwrap(), crate::OrdStatus::REJECTED);

    let res = crate::OrdStatus::from_char('F');
    assert!(!res.is_ok());

    let res = crate::OrdStatus::from_char('=');
    assert!(!res.is_ok());
    assert_eq!(res.err().unwrap(), "Invalid value for enum OrdStatus: '='");

    let res = crate::OrdStatus::from_char(' ');
    assert!(!res.is_ok());
}

pub mod fix50 {

    pub struct SenderCompId(pub String);
    impl SenderCompId {
        pub fn serialize(&self, buf: &mut Vec<u8>) -> Result<(), String> {
            buf.extend(
                format!("49={}\x01", self.0).as_bytes()
            );
            Ok(())
        }
    }

    pub struct TargetCompId(pub String);
    impl TargetCompId {
        pub fn serialize(&self, buf: &mut Vec<u8>) -> Result<(), String> {
            buf.extend(
                format!("56={}\x01", self.0).as_bytes()
            );
            Ok(())
        }
    }

    pub struct SessionContext {
        pub sender_comp_id: SenderCompId,
        pub target_comp_id: TargetCompId,
        pub begin_string: Vec<u8>,
    }

    pub struct Logon {
        pub encrypt_method: crate::EncryptMethod,
        pub heart_bt_int: crate::HeartBtInt,
        pub username: Option<crate::Username>,
        pub password: Option<crate::Password>,
    }
    impl Logon {
        pub fn new(encrypt_method: crate::EncryptMethod, heart_bt_int: crate::HeartBtInt) -> Logon {
            Logon{
                encrypt_method,
                heart_bt_int,
                username: None,
                password: None,
            }
        }
        //FIXME SeqNum must be positive, not sure if zero is allowed.
        pub fn serialize(&self, buf: &mut Vec<u8>, ctx: &SessionContext, seq_num: usize) -> Result<(), String> {

            let mut tmp: Vec<u8> = vec![];

            //  Write message header.
            crate::serialize_msg_type(&mut tmp, "A");
            crate::serialize_msg_seq_num(&mut tmp, seq_num);
            ctx.sender_comp_id.serialize(&mut tmp);
            crate::serialize_sending_time(&mut tmp);
            ctx.target_comp_id.serialize(&mut tmp);

            //  Write message optional/mandatory fields.
            self.encrypt_method.serialize(&mut tmp);
            self.heart_bt_int.serialize(&mut tmp);

            buf.extend(&ctx.begin_string);
            crate::serialize_body_length(buf, tmp.len());
            buf.extend(tmp);

            crate::serialize_checksum(buf);
            Ok(())
        }
    }
}

#[test]
fn encode_logon() {

    let ctx = fix50::SessionContext {
        sender_comp_id: fix50::SenderCompId("EXEC".to_string()),
        target_comp_id: fix50::TargetCompId("BANZAI".to_string()),
        begin_string: b"8=FIX.4.1\x01".to_vec(),
    };

    let mut logon = fix50::Logon::new(
        crate::EncryptMethod::NONE,
        crate::HeartBtInt(30),
    );
    logon.username = Some(crate::Username::new("user"));

    let mut bytes: Vec<u8> = vec![];
    let res = logon.serialize(&mut bytes, &ctx, 1);
    assert!(res.is_ok());

    println!("{}", String::from_utf8_lossy(&bytes));
    let re = ::regex::bytes::Regex::new(r"8=FIX.4.1\x019=61\x0135=A\x0134=1\x0149=EXEC\x0152=\d{8}-\d{2}:\d{2}:\d{2}\x0156=BANZAI\x0198=0\x01108=30\x0110=\d{3}\x01").unwrap();
    assert!(re.is_match(&bytes));
}

