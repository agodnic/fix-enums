
fn main() {
    let out_dir = ::std::env::var("OUT_DIR").unwrap();

    ::nfix_codegen::generate_from_spec(
            ::nfix_spec::FIX44_XML,
            &format!("{}/fix44.rs", out_dir))
        .unwrap();
}
