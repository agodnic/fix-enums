
fn main() {
    use clap::{App, Arg};

    let matches = App::new("spec_to_rust")
                    .about("Parse a FIX protocol spec in XML format, and generate message/field definitions in Rust.")
                    .arg(Arg::with_name("INPUT_FILE")
                        .help("Sets the input file to use")
                        .required(true)
                        .index(1)
                        )
                    .arg(Arg::with_name("OUTPUT_FILE")
                        .help("Name of the output file")
                        .required(true)
                        .index(2)
                        )
                    .get_matches();

    // Calling .unwrap() is safe here because these parameters are mandatory.
    let input_file = matches.value_of("INPUT_FILE").unwrap();
    let output_file = matches.value_of("OUTPUT_FILE").unwrap();

    match ::nfix_codegen::generate_from_file(input_file, output_file) {
        Ok(()) => (),
        Err(e) => println!("Error: {}", e),
    };
}
