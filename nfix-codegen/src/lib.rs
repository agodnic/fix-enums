#[macro_use] extern crate quote;

pub fn generate_from_file<P: AsRef<std::path::Path>>(path_to_spec: P, output_file: &str) -> Result<(), String> {
    let message_spec = ::nfix_spec::fix_spec_from_file(path_to_spec)?;
    generate_internal(&message_spec, output_file)
}

pub fn generate_from_spec(xml: &str, output_file: &str) -> Result<(), String> {
    let message_spec = ::nfix_spec::fix_spec_from_xml(xml)?;
    generate_internal(&message_spec, output_file)
}

fn generate_internal(spec: &::nfix_spec::MessageSpecification, output_file: &str) -> Result<(), String> {

    let mut file_tokens = quote!{

        fn serialize_body_length(buf: &mut Vec<u8>, len: usize) {
            let body_length = format!("9={}\x01", len);
            buf.extend(body_length.as_bytes());
        }

        pub fn serialize_checksum(buf: &mut Vec<u8>) {
            let sum: u32 = buf.iter().fold(0, |sum, x| sum + (*x as u32));
            let checksum = format!("10={:03}\x01", sum % 256);
            buf.extend(checksum.as_bytes());
        }

        pub fn serialize_msg_type(buf: &mut Vec<u8>, msg_type: &str) {
            let s = format!("35={}\x01", msg_type);
            buf.extend(s.as_bytes());
        }

        pub fn serialize_msg_seq_num(buf: &mut Vec<u8>, seq: usize) {
            let s = format!("34={}\x01", seq);
            buf.extend(s.as_bytes());
        }

        fn serialize_sending_time(buf: &mut Vec<u8>) {
            let now: ::chrono::DateTime<::chrono::Utc> = ::chrono::Utc::now();
            let sending_time = now.format("52=%Y%m%d-%H:%M:%S\x01").to_string();
            buf.extend(sending_time.bytes());
        }

    };

    for field in &spec.fields {

        let tokens = match field.ty.as_ref() {
            "CHAR" => crate::emit_char_type(&field),
            "INT" => crate::emit_int_type(&field),
            "STRING" => crate::emit_string_type(&field),
            _ => proc_macro2::TokenStream::new(),
        };

        file_tokens.extend(tokens);
    }

    ::std::fs::write(output_file, file_tokens.to_string())
        .map_err( |e| format!("Failed to write output file: {}", e))?;

    std::process::Command::new("rustfmt").arg(output_file).status()
        .map_err( |e| format!("rustfmt failed: {}", e))?;

    Ok(())
}

fn emit_int_type(field: &::nfix_spec::Field) -> ::proc_macro2::TokenStream {

    let ty_name = format_ident!("{}", field.name);

    let mut variant_tokens = proc_macro2::TokenStream::new();
    if let Some(variants) = &field.variants {
        for variant in variants {
            let variant_name = if variant.name.chars().next().unwrap().is_alphabetic() {
                format_ident!("{}", variant.name)
            } else {
                format_ident!("_{}", variant.name)
            };
            let variant_value: i64 = variant.value.clone().parse().unwrap();
            variant_tokens.extend(quote!{
                pub const #variant_name: #ty_name = #ty_name(#variant_value);
            });
        }
    };

    let tag = field.tag;

    quote! {
        #[derive(Debug, PartialEq, ::serde::Deserialize, ::serde::Serialize)]
        pub struct #ty_name(i64);
        impl #ty_name {
            #variant_tokens
            pub fn serialize(&self, buf: &mut Vec<u8>) -> Result<(), String> {
                buf.extend(
                    format!("{}={}\x01", #tag, self.0).as_bytes()
                );
                Ok(())
            }
        }
    }
}

fn emit_string_type(field: &::nfix_spec::Field) -> ::proc_macro2::TokenStream {

    let ty_name = format_ident!("{}", field.name);

    let mut variant_tokens = proc_macro2::TokenStream::new();
    if let Some(variants) = &field.variants {
        for variant in variants {
            let variant_name = format_ident!("{}_{}", field.name.to_uppercase(), variant.name.to_uppercase());
            let variant_value = variant.value.clone();
            variant_tokens.extend(quote!{
                pub static ref #variant_name: #ty_name = #ty_name(#variant_value.to_string());
            });
        }
    };

    //NOTE We're missing the constructor check for enum types.
    quote! {
        #[derive(Debug, PartialEq, ::serde::Deserialize, ::serde::Serialize)]
        pub struct #ty_name(String);
        ::lazy_static::lazy_static! {
            #variant_tokens
        }
        impl #ty_name {
            pub fn new(val: &str) -> #ty_name {
                #ty_name(val.to_string())
            }
        }
    }
}

fn emit_char_type(field: &::nfix_spec::Field) -> ::proc_macro2::TokenStream {

    let enum_name = format_ident!("{}", field.name);

    let mut variant_tokens = proc_macro2::TokenStream::new();
    if let Some(variants) = &field.variants {
        for variant in variants {
            let variant_name = if variant.name.chars().next().unwrap().is_alphabetic() {
                format_ident!("{}", variant.name)
            } else {
                format_ident!("_{}", variant.name)
            };
            let variant_value = variant.value.as_bytes()[0] as char;
            variant_tokens.extend(quote!{
                pub const #variant_name: #enum_name = #enum_name(#variant_value);
            });
        }
    };

    let check = if let Some(variants) = &field.variants {
        let variants = variants.iter().map( |v| v.value.as_bytes()[0] as char );
        let len = variants.len();
        let name = &field.name;
        quote!{
            let variants: [char; #len] = [#(#variants),*];
            if !variants.contains(&c) {
                return Err(format!("Invalid value for enum {}: '{}'", #name, c))
            }
        }
    } else {
        quote!{ }
    };

    quote!{
        /// doc test
        #[derive(Debug, Clone, PartialEq, Eq, ::serde::Deserialize, ::serde::Serialize)]
        pub struct #enum_name(char);
        impl #enum_name {
            #variant_tokens
            pub fn to_char(&self) -> char {
                self.0
            }
            pub fn from_char(c: char) -> Result<#enum_name, String> {
                #check
                Ok(#enum_name(c))
            }
        }
    }
}

//#[cfg(test)]
//mod tests {
//
//    #[test]
//    fn q() {
//        let tokens = crate::emit_char_type("Side");
//        let expected =
//            "# [ doc = r\" doc test\" ] \
//            pub struct Side ( char ) ; \
//            impl Side { \
//            pub const BUY : Side = Side ( '1' ) ; \
//            pub const SELL : Side = Side ( '2' ) ; \
//            }";
//
//        assert_eq!(tokens.to_string(), expected);
//    }
//
//}
