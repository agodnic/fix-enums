/// Convenience helper functions for XML parsing
use crate::Result;


/// Return the first child of a node with a given tag name.
pub fn get_child_by_name<'a, 'b>(node: &::roxmltree::Node<'a, 'b>, name: &str) -> Result<::roxmltree::Node<'a, 'b>> {

    for child in node.children().filter( |n| n.is_element() && n.tag_name().name()==name ) {
        return Ok(child)
    }

    Err(format!("Couldn't find child \"{}\"", name))
}

/// Return all childs of a node with a given tag name.
pub fn get_childs_with_name<'a, 'b>(node: &::roxmltree::Node<'a, 'b>, name: &str) -> Result<Vec<::roxmltree::Node<'a, 'b>>> {
    Ok(
        node.children().filter( |n| n.is_element() && n.tag_name().name()==name ).collect()
    )
}

/// Retrieve an attribute from a node, return it as a String.
pub fn parse_string_attribute(
    node: &::roxmltree::Node,
    attribute_name: &str,
) -> Result<String> {

    match node.attribute(attribute_name) {
        Some(s) => Ok(s.to_string()),
        None => Err(format!("Failed to read \"{}\" attribute from value node", attribute_name)),
    }
}

/// Retrieve an attribute from a node and convert it to an usize.
pub fn parse_usize_attribute(
    node: &::roxmltree::Node,
    attribute_name: &str,
) -> Result<usize> {

    let s = parse_string_attribute(node, attribute_name)?;

    match s.parse::<usize>() {
        Ok(number) => Ok(number),
        Err(e) => return Err(format!("Failed to parse \"{}\" attribute on field node: {}", attribute_name, e)),
    }
}
