#[cfg(test)]

#[test]
fn fix40_specification() {

    let res = crate::fix40_spec();
    assert!(res.is_ok());

    let spec = res.unwrap();

    let fields = spec.fields;
    assert_eq!(fields.len(), 138);

    let account = &fields[0];
    assert_eq!(account.name, "Account");
    assert_eq!(account.tag, 1);
    assert_eq!(account.ty, "CHAR");
    assert_eq!(account.variants, None);
}

#[test]
fn fix41_specification() {

    let res = crate::fix41_spec();
    assert!(res.is_ok());

    let spec = res.unwrap();

    let fields = spec.fields;
    assert_eq!(fields.len(), 206);

    let account = &fields[0];
    assert_eq!(account.name, "Account");
    assert_eq!(account.tag, 1);
    assert_eq!(account.ty, "CHAR");
    assert_eq!(account.variants, None);
}

#[test]
fn fix42_specification() {

    let res = crate::fix42_spec();
    assert!(res.is_ok());

    let spec = res.unwrap();

    let fields = spec.fields;
    assert_eq!(fields.len(), 405);

    let account = &fields[0];
    assert_eq!(account.name, "Account");
    assert_eq!(account.tag, 1);
    assert_eq!(account.ty, "STRING");
    assert_eq!(account.variants, None);
}

#[test]
fn fix43_specification() {

    let res = crate::fix43_spec();
    assert!(res.is_ok());

    let spec = res.unwrap();

    let fields = spec.fields;
    assert_eq!(fields.len(), 635);

    let account = &fields[0];
    assert_eq!(account.name, "Account");
    assert_eq!(account.tag, 1);
    assert_eq!(account.ty, "STRING");
    assert_eq!(account.variants, None);
}

#[test]
fn fix44_specification() {

    let res = crate::fix44_spec();
    assert!(res.is_ok());

    let spec = res.unwrap();

    let fields = spec.fields;
    assert_eq!(fields.len(), 912);

    let account = &fields[0];
    assert_eq!(account.name, "Account");
    assert_eq!(account.tag, 1);
    assert_eq!(account.ty, "STRING");
    assert_eq!(account.variants, None);
}

#[test]
fn fix50_specification() {

    let res = crate::fix50_spec();
    assert!(res.is_ok());

    let spec = res.unwrap();

    let fields = spec.fields;
    assert_eq!(fields.len(), 1090);

    let account = &fields[0];
    assert_eq!(account.name, "Account");
    assert_eq!(account.tag, 1);
    assert_eq!(account.ty, "STRING");
    assert_eq!(account.variants, None);
}

#[test]
fn fix50sp1_specification() {

    let res = crate::fix50sp1_spec();
    assert!(res.is_ok());

    let spec = res.unwrap();

    let fields = spec.fields;
    assert_eq!(fields.len(), 1373);

    let account = &fields[0];
    assert_eq!(account.name, "Account");
    assert_eq!(account.tag, 1);
    assert_eq!(account.ty, "STRING");
    assert_eq!(account.variants, None);
}

#[test]
fn fix50sp2_specification() {

    let res = crate::fix50sp2_spec();
    assert!(res.is_ok());

    let spec = res.unwrap();

    let fields = spec.fields;
    assert_eq!(fields.len(), 1452);

    let account = &fields[0];
    assert_eq!(account.name, "Account");
    assert_eq!(account.tag, 1);
    assert_eq!(account.ty, "STRING");
    assert_eq!(account.variants, None);
}

#[test]
fn fixt11_specification() {

    let res = crate::fixt11_spec();
    assert!(res.is_ok());

    let spec = res.unwrap();

    let fields = spec.fields;
    assert_eq!(fields.len(), 62);

    let account = &fields[0];
    assert_eq!(account.name, "BeginSeqNo");
    assert_eq!(account.tag, 7);
    assert_eq!(account.ty, "SEQNUM");
    assert_eq!(account.variants, None);
}
