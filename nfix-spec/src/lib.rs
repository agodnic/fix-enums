mod tests;
mod xml_helpers;

pub static FIX40_XML:    &str = include_str!("spec/FIX40.xml");
pub static FIX41_XML:    &str = include_str!("spec/FIX41.xml");
pub static FIX42_XML:    &str = include_str!("spec/FIX42.xml");
pub static FIX43_XML:    &str = include_str!("spec/FIX43.xml");
pub static FIX44_XML:    &str = include_str!("spec/FIX44.xml");
pub static FIX50_XML:    &str = include_str!("spec/FIX50.xml");
pub static FIX50SP1_XML: &str = include_str!("spec/FIX50SP1.xml");
pub static FIX50SP2_XML: &str = include_str!("spec/FIX50SP2.xml");
pub static FIXT11_XML:   &str = include_str!("spec/FIXT11.xml");

pub type Result<T> = ::std::result::Result<T, String>;


/// Metadata that describes a field in the FIX specification.
#[derive(Debug, PartialEq)]
pub struct Field {
    /// Name of the field.
    pub name: String,
    /// The field's tag number (used for tagvalue encoding).
    pub tag: usize,
    /// FIX data type for this field.
    pub ty: String,
    /// If the field is an enum, this describes the enum variants for the field.
    pub variants: Option<Vec<EnumVariant>>,
}

/// For enum fields, this defines the value of a single enum variant.
#[derive(Debug, PartialEq)]
pub struct EnumVariant {
    pub value: String,
    pub name: String,
}

/// Field, component, group and message definitions for a specific FIX protocol version.
#[derive(Debug, PartialEq)]
pub struct MessageSpecification {
    pub fields: Vec<Field>,
}


/// Parses the specification DOM, and returns a high-level representation of the spec.
fn parse_spec(root: &::roxmltree::Node) -> Result<MessageSpecification> {

    let fields_node = xml_helpers::get_child_by_name(root, "fields")?;
    Ok(
        MessageSpecification {
            fields: parse_fields(&fields_node)?,
        }
    )
}


/// Parses all information contained in the <fields> XML node.
fn parse_fields(
    fields_node: &::roxmltree::Node,
) -> Result<Vec<Field>> {

    let mut fields = vec![];

    for field_node in xml_helpers::get_childs_with_name(fields_node, "field")? {
        let field = Field {
            tag: xml_helpers::parse_usize_attribute(&field_node, "number")?,
            name: xml_helpers::parse_string_attribute(&field_node, "name")?,
            ty: xml_helpers::parse_string_attribute(&field_node, "type")?,
            variants: parse_enum_variants(&field_node)?,
        };
        fields.push(field);
    }

    Ok(fields)
}


/// Parses all <value> tags inside a <field> tag.
fn parse_enum_variants(
    field_node: &::roxmltree::Node
) -> Result<Option<Vec<EnumVariant>>> {

    let mut variants = vec![];

    for value_node in xml_helpers::get_childs_with_name(field_node, "value")? {
        variants.push(
            EnumVariant {
                value: xml_helpers::parse_string_attribute(&value_node, "enum")?,
                name: xml_helpers::parse_string_attribute(&value_node, "description")?,
            }
        );
    }

    if variants.len() > 0 {
        Ok(Some(variants))
    } else {
        Ok(None)
    }
}

/// Returns a high-level representation of the FIX 4.0 specification.
pub fn fix_spec_from_file<P: AsRef<std::path::Path>>(path: P) -> Result<MessageSpecification> {

    let xml = ::std::fs::read_to_string(path)
        .map_err( |e| format!("Failed to read spec file: {}", e))?;

    fix_spec_from_xml(&xml)
}

/// Returns a high-level representation of the FIX 4.0 specification.
pub fn fix_spec_from_xml(text: &str) -> Result<MessageSpecification> {

    let doc = roxmltree::Document::parse(&text)
        .map_err( |e| format!("call to roxmltree::Document::parse failed - {}", e) )?;

    parse_spec(&doc.root_element())
}


/// Returns a high-level representation of the FIX 4.0 specification.
pub fn fix40_spec() -> Result<MessageSpecification> {
    fix_spec_from_xml(FIX40_XML)
}

/// Returns a high-level representation of the FIX 4.1 specification.
pub fn fix41_spec() -> Result<MessageSpecification> {
    fix_spec_from_xml(FIX41_XML)
}

/// Returns a high-level representation of the FIX 4.2 specification.
pub fn fix42_spec() -> Result<MessageSpecification> {
    fix_spec_from_xml(FIX42_XML)
}

/// Returns a high-level representation of the FIX 4.3 specification.
pub fn fix43_spec() -> Result<MessageSpecification> {
    fix_spec_from_xml(FIX43_XML)
}

/// Returns a high-level representation of the FIX 4.4 specification.
pub fn fix44_spec() -> Result<MessageSpecification> {
    fix_spec_from_xml(FIX44_XML)
}

/// Returns a high-level representation of the FIX 5.0 specification.
pub fn fix50_spec() -> Result<MessageSpecification> {
    fix_spec_from_xml(FIX50_XML)
}

/// Returns a high-level representation of the FIX 5.0 SP1 specification.
pub fn fix50sp1_spec() -> Result<MessageSpecification> {
    fix_spec_from_xml(FIX50SP1_XML)
}

/// Returns a high-level representation of the FIX 5.0 SP2 specification.
pub fn fix50sp2_spec() -> Result<MessageSpecification> {
    fix_spec_from_xml(FIX50SP2_XML)
}

/// Returns a high-level representation of the FIXT 1.1 specification.
pub fn fixt11_spec() -> Result<MessageSpecification> {
    fix_spec_from_xml(FIXT11_XML)
}
