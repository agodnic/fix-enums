# fix-spec

Programatically access specifications of the Financial Information eXchange (FIX) protocol.
All protocol versions are supported.

This crate is mostly designed to aid code generation.


## How this crate works

This crate parses specification files that define the messages/fields used in each FIX protocol version.
The format of these files is [the same that the QuickFix library](https://github.com/quickfix/quickfix/tree/fb2e3cf8f9cdef6e66c546d590797e903052d76a/spec) uses.

