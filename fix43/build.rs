
fn main() {
    let out_dir = ::std::env::var("OUT_DIR").unwrap();

    ::nfix_codegen::generate_from_spec(
            ::nfix_spec::FIX43_XML,
            &format!("{}/fix43.rs", out_dir))
        .unwrap();
}
