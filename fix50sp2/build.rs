
fn main() {
    let out_dir = ::std::env::var("OUT_DIR").unwrap();

    ::nfix_codegen::generate_from_spec(
            ::nfix_spec::FIX50SP2_XML,
            &format!("{}/fix50sp2.rs", out_dir))
        .unwrap();
}
