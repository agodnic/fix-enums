
#[derive(serde::Deserialize, configure::Configure, Debug)]
pub struct Config {
    pub host: String,
    pub port: u16,
}

::lazy_static::lazy_static! {
    /// Global configuration for the connector.
    pub static ref CONFIG: crate::config::Config = {

        use configure::Configure;

        ::configure::use_default_config!();

        match crate::config::Config::generate() {
            Ok(c) => c,
            Err(e) => panic!("Failed to initialize configuration: {}", e),
        }
    };
}
