#[macro_use]
extern crate configure;

pub mod config;
use config::CONFIG;

//NOTE Run this as  INITIATOR_HOST=a.b.com INITIATOR_PORT=666 cargo run --bin initiator
fn main() -> Result<(), String> {
    println!("{:?}", *CONFIG);

    let mut stream = connect_tls(&CONFIG.host, CONFIG.port)?;
    stream
        .get_mut()
        .set_read_timeout(Some(::std::time::Duration::new(0, 1000*1000)))
        .map_err( |e| format!("Failed to set socket read timeout: {}", e))?;

    let mut session = Session::new();

    loop {
        if session.wants_write_fix() {
            session.write_fix(&mut stream)?
        }
        if session.wants_read_fix() {
            session.read_fix(&mut stream)?;
            //NOTE Not sure how we'll process incoming messages.
            // I think we were gonna use callbacks somehow.
            // So I'm not sure if this meant like connection / session events
            //  or what.
            session.process()?;
        }
    }

}

pub fn connect_tls(host: &str, port: u16) -> Result<::native_tls::TlsStream<::std::net::TcpStream>, String> {

    let mut builder = ::native_tls::TlsConnector::builder();
    builder.danger_accept_invalid_certs(true);

    let connector = builder.build()
        .map_err( |e| format!("Failed to create ::native_tls::Connector: {}", e))?;

    let stream = ::std::net::TcpStream::connect((host,port))
        .map_err( |e| format!("TcpStream::connect failed: {}", e))?;
    let stream = connector.connect(host, stream)
        .map_err( |e| format!("::native_tls::Connector::connect failed: {}", e))?;

    Ok(stream)
}

struct Session {
}
enum Event {
    NotSureIfEventsAreJustConnectionOrSessionStuffOrWhat
}

impl Session {
    pub fn new() -> Session {

        //use ::nfix_msg::fix50;

        //let mut logon = fix50::Logon::new(
        //  fix50::EncryptMethod::None,
        //  fix50::HeartBtInt(30)
        //);
        //logon.username = Some(fix50::Username("foo".to_string()));

        //let buf = vec![];
        //logon.serialize(&buf);

        Session {}
    }
    pub fn wants_read_fix(&self) -> bool {
        unimplemented!()
    }
    pub fn read_fix(&mut self, _rd: &mut dyn ::std::io::Read) -> Result<(), String> {
        unimplemented!()
    }
    pub fn wants_write_fix(&self) -> bool {
        unimplemented!()
    }
    pub fn write_fix(&mut self, _wr: &mut dyn ::std::io::Write) -> Result<(), String> {
        unimplemented!()
    }
    pub fn process(&mut self) -> Result<Option<Event>, String> {
        unimplemented!()
    }
}
