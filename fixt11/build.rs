
fn main() {
    let out_dir = ::std::env::var("OUT_DIR").unwrap();

    ::nfix_codegen::generate_from_spec(
            ::nfix_spec::FIXT11_XML,
            &format!("{}/fixt11.rs", out_dir))
        .unwrap();
}
